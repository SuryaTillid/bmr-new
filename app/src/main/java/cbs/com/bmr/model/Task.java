package cbs.com.bmr.model;

import java.util.ArrayList;

/*********************************************************************
 * Created by Barani on 29-03-2019 in TableMateNew
 ***********************************************************************/
public class Task {
    private String task_id;
    private String task_date;
    private String created_by_id;
    private String created_date;
    private ArrayList<TaskScheduler> taskSchedule = new ArrayList<>();

    public String getTask_id() {
        return task_id;
    }

    public void setTask_id(String task_id) {
        this.task_id = task_id;
    }

    public String getTask_date() {
        return task_date;
    }

    public void setTask_date(String task_date) {
        this.task_date = task_date;
    }

    public String getCreated_by_id() {
        return created_by_id;
    }

    public void setCreated_by_id(String created_by_id) {
        this.created_by_id = created_by_id;
    }

    public String getCreated_date() {
        return created_date;
    }

    public void setCreated_date(String created_date) {
        this.created_date = created_date;
    }

    public ArrayList<TaskScheduler> getTaskSchedule() {
        return taskSchedule;
    }

    public void setTaskSchedule(ArrayList<TaskScheduler> taskSchedule) {
        this.taskSchedule = taskSchedule;
    }
}

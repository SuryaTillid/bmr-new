package cbs.com.bmr.model;

/*********************************************************************
 * Created by Barani on 09-05-2019 in TableMateNew
 ***********************************************************************/
public class ManagementRequestList {
    private String id;
    private String req_desc;
    private String req_dep;
    private String req_date;
    private String req_by;
    private String req_qty;
    private String req_time;
    private String approved_qty;
    private String expected_budget;
    private String approved_budget;
    private String comments;
    private String approval_status;
    private String approved_by;
    private String approval_date;
    private String status_comments;
    private String department_name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReq_desc() {
        return req_desc;
    }

    public void setReq_desc(String req_desc) {
        this.req_desc = req_desc;
    }

    public String getReq_dep() {
        return req_dep;
    }

    public void setReq_dep(String req_dep) {
        this.req_dep = req_dep;
    }

    public String getReq_date() {
        return req_date;
    }

    public void setReq_date(String req_date) {
        this.req_date = req_date;
    }

    public String getReq_by() {
        return req_by;
    }

    public void setReq_by(String req_by) {
        this.req_by = req_by;
    }

    public String getReq_qty() {
        return req_qty;
    }

    public void setReq_qty(String req_qty) {
        this.req_qty = req_qty;
    }

    public String getApproved_qty() {
        return approved_qty;
    }

    public void setApproved_qty(String approved_qty) {
        this.approved_qty = approved_qty;
    }

    public String getExpected_budget() {
        return expected_budget;
    }

    public void setExpected_budget(String expected_budget) {
        this.expected_budget = expected_budget;
    }

    public String getApproved_budget() {
        return approved_budget;
    }

    public void setApproved_budget(String approved_budget) {
        this.approved_budget = approved_budget;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getApproval_status() {
        return approval_status;
    }

    public void setApproval_status(String approval_status) {
        this.approval_status = approval_status;
    }

    public String getApproved_by() {
        return approved_by;
    }

    public void setApproved_by(String approved_by) {
        this.approved_by = approved_by;
    }

    public String getApproval_date() {
        return approval_date;
    }

    public void setApproval_date(String approval_date) {
        this.approval_date = approval_date;
    }

    public String getStatus_comments() {
        return status_comments;
    }

    public void setStatus_comments(String status_comments) {
        this.status_comments = status_comments;
    }

    public String getDepartment_name() {
        return department_name;
    }

    public void setDepartment_name(String department_name) {
        this.department_name = department_name;
    }

    public String getReq_time() {
        return req_time;
    }

    public void setReq_time(String req_time) {
        this.req_time = req_time;
    }
}

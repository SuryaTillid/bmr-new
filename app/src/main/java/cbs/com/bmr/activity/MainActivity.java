package cbs.com.bmr.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import java.util.List;

import cbs.com.bmr.Helper.ConfigurationSettings;
import cbs.com.bmr.R;

/*********************************************************************
 * Created by Barani on 22-03-2019 in TableMateNew
 ***********************************************************************/
public class MainActivity extends AppCompatActivity implements FragmentDrawer.FragmentDrawerListener {

    private Toolbar mToolbar;
    private FragmentDrawer drawerFragment;
    private Context context;
    private ConfigurationSettings settings;
    private String userName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        context = MainActivity.this;
        settings = new ConfigurationSettings(context);
        mToolbar = findViewById(R.id.toolbar);

        userName = settings.getUSER_NAME();
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        drawerFragment = (FragmentDrawer) getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), mToolbar);
        drawerFragment.setDrawerListener(this);

        // display the first navigation drawer view on app launch
        displayView(4);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_sign_out, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement

        if(id == R.id.action_sign_out)
        {
            exitFromApp();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void exitFromApp() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);
        builder.setMessage("Are you sure want to Logout, your session will be cleared?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent i = new Intent(context, LoginActivity.class);
                startActivity(i);
                settings.clearSession();
            }
        });

        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //if user select "No", just cancel this dialog and continue with app
                dialog.cancel();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onDrawerItemSelected(View view, int position) {
        displayView(position);
    }

    public void setActionBarTitle(String title) {
        getSupportActionBar().setTitle(title);
    }

    public void displayView(int position) {
        Fragment fragment = null;
        String title = getString(R.string.app_name);
        switch (position) {
            case 0:
                fragment = new AttendanceFragment();
                title = getString(R.string.title_attendance);
                break;
            case 1:
                fragment = new ChangePasswordFragment();
                title = getString(R.string.title_change_password);
                break;
            case 2:
                fragment = new ManagementRequest();
                title = getString(R.string.title_mgmt_request);
                break;
            case 3:
                fragment = new ManagementActivity_TaskApproval();
                title = getString(R.string.title_task_approval);
                break;
            case 4:
                CurrentDateTaskFragment fragment_c = new CurrentDateTaskFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.container_body, fragment_c).commit();
                title = getString(R.string.title_today_task);
                getSupportActionBar().setTitle(title);
                break;
            case 5:
                fragment = new TaskListFragment();
                title = getString(R.string.title_home);
                break;
            case 6:
                fragment = new ManagementActivity_TaskCreate();
                title = getString(R.string.title_task_scheduler);
                break;
            case 7:
                fragment = new ToDoList_Fragment();
                title = getString(R.string.title_todo_list);
                break;
            default:
                break;
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.commit();
            // set the toolbar title
            getSupportActionBar().setTitle(title);
        }
    }

    @Override
    public void onBackPressed() {
        callFragments();
    }

    private void callFragments() {
        List<Fragment> fragments = getSupportFragmentManager().getFragments();
        for (Fragment f : fragments) {
            if (f instanceof TaskSchedulerFragment)
                ((TaskSchedulerFragment) f).onBackPressed();
            if (f instanceof TaskUpdateFragment)
                ((TaskUpdateFragment) f).onBackPressed();
            if (f instanceof AttendanceFragment)
                ((AttendanceFragment) f).onBackPressed();
            if (f instanceof CurrentDateTaskFragment)
                ((CurrentDateTaskFragment) f).onBackPressed();
            if (f instanceof CreateTaskForTodayFragment)
                ((CreateTaskForTodayFragment) f).onBackPressed();
            if (f instanceof TaskListFragment)
                ((TaskListFragment) f).onBackPressed();
            if (f instanceof FeedbackFromCustomer)
                ((FeedbackFromCustomer) f).onBackPressed();
            if (f instanceof TaskUpdateFragmentForCurrentDayFragment)
                ((TaskUpdateFragmentForCurrentDayFragment) f).onBackPressed();
            if (f instanceof ToDoList_Fragment)
                ((ToDoList_Fragment) f).onBackPressed();
            if (f instanceof ManagementRequest)
                ((ManagementRequest) f).onBackPressed();
            if (f instanceof ChangePasswordFragment)
                ((ChangePasswordFragment) f).onBackPressed();
            if (f instanceof ManagementActivity_TaskCreate)
                ((ManagementActivity_TaskCreate) f).onBackPressed();
            if (f instanceof ManagementCreateTaskForTodayFragment)
                ((ManagementCreateTaskForTodayFragment) f).onBackPressed();
            if (f instanceof ManagementActivity_TaskApproval)
                ((ManagementActivity_TaskApproval) f).onBackPressed();
        }
    }
}
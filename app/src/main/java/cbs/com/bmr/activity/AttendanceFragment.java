package cbs.com.bmr.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.google.gson.Gson;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import cbs.com.bmr.Helper.ConfigurationSettings;
import cbs.com.bmr.Helper.MyCustomDialog;
import cbs.com.bmr.Listener.On_BackPressed;
import cbs.com.bmr.R;
import cbs.com.bmr.configuration.App;
import cbs.com.bmr.configuration.AppLog;
import cbs.com.bmr.configuration.RestApiCalls;
import cbs.com.bmr.model.Location_Details;
import cbs.com.bmr.model.SuccessMessage;

/*********************************************************************
 * Created by Barani on 08-04-2019 in TableMateNew
 *********************************************************************/
public class AttendanceFragment extends Fragment implements View.OnClickListener, LocationListener, On_BackPressed {

    private Context context;
    private Button btn_day_in, btn_day_out;
    private TextView t_check_in_time;
    private String emp_id, date, lat_lon = null,display_time;
    private LinearLayout layout_check_in;
    private ConfigurationSettings settings;
    private EditText edit_start_read,edit_close_read;
    LocationManager locationManager;
    private String DayIN;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.activity_attendance, container, false);
        context = getActivity();
        settings = new ConfigurationSettings(context);
        initControls(view);

        if (ContextCompat.checkSelfPermission(getActivity().getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity().getApplicationContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, 101);
        }

        emp_id = settings.getEmployee_ID();

        new GetDayInStatus().execute();
        getLocation();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).setActionBarTitle("Attendance");
    }

    private void startLocationUpdates() {
        //fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null /* Looper */);
    }

    private void initControls(View view) {
        btn_day_in = view.findViewById(R.id.btn_day_in);
        btn_day_out = view.findViewById(R.id.btn_day_out);
        t_check_in_time = view.findViewById(R.id.t_check_in_time);
        layout_check_in = view.findViewById(R.id.layout_check_in_time);
        edit_start_read = view.findViewById(R.id.edit_start_read);
        edit_close_read = view.findViewById(R.id.edit_close_read);
        btn_day_in.setOnClickListener(this);
        btn_day_out.setOnClickListener(this);
    }

    private void getLocation() {
        try {
            locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 5, this);
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        try {
            String lat = String.valueOf(location.getLatitude());
            String lon = String.valueOf(location.getLongitude());
            if (!TextUtils.isEmpty(lat) && !TextUtils.isEmpty(lon)) {
                lat_lon = new Gson().toJson(getLocationDetails(lat, lon));
                AppLog.write("LAT_LON", lat_lon);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Location_Details getLocationDetails(String lat, String lon) {
        Location_Details location = new Location_Details();
        location.setLatitude(lat);
        location.setLongitude(lon);
        return location;
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onProviderDisabled(String provider) {
    }

    @Override
    public void onClick(View v) {
       // getLocation();
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat date_format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat display_date_format = new SimpleDateFormat("dd-MMM-yyyy hh:mm a");
        date = date_format.format(calendar.getTime());
        display_time = display_date_format.format(calendar.getTime());
        if (!TextUtils.isEmpty(lat_lon) && !TextUtils.isEmpty(date)) {
            switch (v.getId()) {
                case R.id.btn_day_in:
                    String start_meter = edit_start_read.getText().toString().trim();
                    new Day_In_submit().execute(emp_id, date, lat_lon,start_meter);
                    break;
                case R.id.btn_day_out:
                    final String close_meter = edit_close_read.getText().toString().trim();
                    if(check_day_out(date)>1)
                    {
                        new Day_Out_submit().execute(emp_id, date, lat_lon,close_meter);
                    }
                    else
                    {
                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setCancelable(false);
                        builder.setMessage("Are you sure want to checkout?");
                        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //if user select "No", just cancel this dialog and continue with app
                                dialog.cancel();
                            }
                        });
                        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                new Day_Out_submit().execute(emp_id, date, lat_lon,close_meter);
                            }
                        });
                        AlertDialog alert = builder.create();
                        alert.show();
                    }
                    break;
            }
        } else {
            Toast.makeText(context,"Please Press again..!",Toast.LENGTH_SHORT).show();
            //getLocation();
        }
    }

    private int check_day_out(String date) {
        int hours = 0;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date startDate = simpleDateFormat.parse(date);
            Date endDate = simpleDateFormat.parse(settings.getCHECKED_IN_TIME());
            long difference = endDate.getTime() - startDate.getTime();
            int days = (int) (difference / (1000*60*60*24));
            hours = (int) ((difference - (1000*60*60*24*days)) / (1000*60*60));
            int min = (int) (difference - (1000*60*60*24*days) - (1000*60*60*hours)) / (1000*60);
            AppLog.write("log_tag","Hours: "+hours+", Mins: "+min+" "+days+"-----"+Math.abs(min));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return Math.abs(hours);
    }

    @Override
    public void onBackPressed() {
        AppLog.write("Worked--", "--");
        callHomeFragment();
    }

    private void callHomeFragment() {
        Log.d("Home","Called");
        ((MainActivity)getActivity()).displayView(0);
       /* TaskListFragment fragment = new TaskListFragment();
        if (fragment != null) {
            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.commit();
        }*/
    }

    private class Day_Out_submit extends AsyncTask<String, String, SuccessMessage> {

        MyCustomDialog dialog;
        String result = "";

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected SuccessMessage doInBackground(String... p) {
            RestApiCalls call = new RestApiCalls();
            return call.EmployeeOut_Update_attendance((App) getActivity().getApplication(), p[0],settings.getUPDATE_ID(),
                    p[1], p[2],p[3]);
        }

        @Override
        protected void onPostExecute(SuccessMessage success) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            AppLog.write("CHECK__OUT", "---" + new Gson().toJson(success));
            if (success != null) {
                if (success.getSuccess().equalsIgnoreCase("1")) {
                    Toast.makeText(getActivity(), "Check-out Successfully..!", Toast.LENGTH_SHORT).show();
                    settings.setIS_CHECKED_IN(false);
                    AppLog.write("CHECK__OUT_STATUS", "---" + settings.isCheckIn());
                    btn_day_in.setVisibility(View.VISIBLE);
                    btn_day_out.setVisibility(View.GONE);
                    layout_check_in.setVisibility(View.GONE);
                    edit_close_read.setVisibility(View.GONE);
                    edit_start_read.setVisibility(View.VISIBLE);
                    callHomeFragment();
                } else {
                    Toast.makeText(getActivity(), "Failed to Check-out..!", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(getActivity(), "Failed to Check-out..!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class Day_In_submit extends AsyncTask<String, String, SuccessMessage> {

        MyCustomDialog dialog;
        String result = "";

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected SuccessMessage doInBackground(String... p) {
            RestApiCalls call = new RestApiCalls();
            return call.EmployeeIn_Create_attendance((App) getActivity().getApplication(), p[0], p[1], p[2],p[3]);
        }

        @Override
        protected void onPostExecute(SuccessMessage success) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            if (success != null) {
                if (success.getSuccess().equalsIgnoreCase("1"))
                {
                    //Toast.makeText(getActivity(), "Check-In Successfully..!", Toast.LENGTH_SHORT).show();
                    settings.setIS_CHECKED_IN(true);
                    settings.setCHECKED_IN_TIME(display_time);
                    settings.setUPDATE_ID(success.getUpdate_id());
                    AppLog.write("CHECK_OUT_STATUS", "---1." + settings.isCheckIn());
                    btn_day_out.setVisibility(View.VISIBLE);
                    btn_day_in.setVisibility(View.GONE);
                    layout_check_in.setVisibility(View.VISIBLE);
                    edit_close_read.setVisibility(View.VISIBLE);
                    edit_start_read.setVisibility(View.GONE);
                    t_check_in_time.setText("CheckIn at :  "+settings.getCHECKED_IN_TIME());
                    successCheckIN();
                }
                else {
                    Toast.makeText(getActivity(), "Failed to Check-In..!", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(getActivity(), "Failed to Check-In..!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void successCheckIN() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);
        builder.setMessage("Your Attendance has been recorded successfully");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
               callHomeFragment();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private class GetDayInStatus extends AsyncTask<String, String, SuccessMessage> {

        MyCustomDialog dialog;
        String result = "";

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected SuccessMessage doInBackground(String... p) {
            RestApiCalls call = new RestApiCalls();
            return call.call_for_getDayIn_status((App) getActivity().getApplication(),settings.getEmployee_ID());
        }

        @Override
        protected void onPostExecute(SuccessMessage success) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            AppLog.write("Executed_Day_IN_status","-"+new Gson().toJson(success));
            if(success!=null)
            {
                if(success.getSuccess().equalsIgnoreCase("1"))
                {
                    DayIN = "1";
                    AppLog.write("Check_In","----");
                    btn_day_out.setVisibility(View.VISIBLE);
                    btn_day_in.setVisibility(View.GONE);
                    layout_check_in.setVisibility(View.VISIBLE);
                    edit_close_read.setVisibility(View.VISIBLE);
                    edit_start_read.setVisibility(View.GONE);
                    t_check_in_time.setText("CheckIn at :  "+settings.getCHECKED_IN_TIME());
                }
                else if(success.getSuccess().equalsIgnoreCase("2"))
                {
                    DayIN = "2";
                    AppLog.write("Not_Checked_In","----");
                    btn_day_out.setVisibility(View.GONE);
                    btn_day_in.setVisibility(View.VISIBLE);
                    layout_check_in.setVisibility(View.GONE);
                    edit_close_read.setVisibility(View.GONE);
                    edit_start_read.setVisibility(View.VISIBLE);
                }
                else if(success.getSuccess().equalsIgnoreCase("0"))
                {
                    DayIN = "0";
                    AppLog.write("Check_In","----"+DayIN);
                }
            }
            else {
                AppLog.write("Day_IN_status", "Not Working");
            }
        }
    }
}
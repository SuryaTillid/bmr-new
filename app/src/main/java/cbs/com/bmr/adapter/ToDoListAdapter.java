package cbs.com.bmr.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import cbs.com.bmr.Listener.ToDoListener;
import cbs.com.bmr.R;
import cbs.com.bmr.model.TODO_Taskslist;

/*********************************************************************
 * Created by Barani on 03-05-2019 in TableMateNew
 ***********************************************************************/
public class ToDoListAdapter extends RecyclerView.Adapter<ToDoListAdapter.MyHolder> {
    private ArrayList<TODO_Taskslist> taskLists;
    private Context context;
    private LayoutInflater inflater;
    private ToDoListener listener;

    public ToDoListAdapter(Context context, ArrayList<TODO_Taskslist> taskList) {
        this.context = context;
        this.taskLists = taskList;
        inflater = LayoutInflater.from(context);
    }

    public void MyDataChanged(ArrayList<TODO_Taskslist> list) {
        taskLists = list;
        notifyDataSetChanged();
    }

    public void setTaskListListener(ToDoListener listener) {
        this.listener = listener;
    }

    @Override
    public ToDoListAdapter.MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.activity_todo_list_row, parent, false);
        return new MyHolder(v);
    }

    @Override
    public void onBindViewHolder(ToDoListAdapter.MyHolder holder, int position) {
        TODO_Taskslist taskList = taskLists.get(position);

        holder.t_content.setText(taskList.getTask_name());
        holder.t_date.setText(date_validate(taskList.getTask_due()));

    }

    private String date_validate(String task_date) {
        try {
            if (!TextUtils.isEmpty(task_date)) {
                DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date date = format.parse(task_date);
                DateFormat date_formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm a");
                task_date = date_formatter.format(date);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return task_date;
    }

    @Override
    public int getItemCount() {
        return taskLists.size();
    }

    public class MyHolder extends RecyclerView.ViewHolder {

        TextView t_content;
        TextView t_date;
        TextView t_edit;

        public MyHolder(View v) {
            super(v);
            t_date = v.findViewById(R.id.t_task_date);
            t_content = v.findViewById(R.id.t_task_title);
            t_edit = v.findViewById(R.id.t_edit);

            t_edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.todoList_click_listener(getAdapterPosition());
                }
            });
        }
    }
}
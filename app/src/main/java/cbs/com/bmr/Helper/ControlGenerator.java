package cbs.com.bmr.Helper;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.InputType;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

/*********************************************************************
 * Created by Barani on 03-04-2019 in TableMateNew
 ***********************************************************************/
public class ControlGenerator {
    private Context context;

    public ControlGenerator(Context context) {
        this.context = context;
    }

    public TextView createTextView(int id, String text) {
        TextView textView = new TextView(context);
        textView.setId(id);
        textView.setTextSize(16);
        textView.setTypeface(Typeface.DEFAULT_BOLD);
        textView.setTextColor(Color.parseColor("#000000"));
        textView.setText(text);
        return textView;
    }

    public EditText createEditText(int id, int drawable, String questionType) {
        EditText edit = new EditText(context);
        edit.setId(id);
        edit.setBackgroundResource(drawable);
        if (questionType.equalsIgnoreCase("0")) {
            edit.setInputType(InputType.TYPE_CLASS_NUMBER);
        } else {
            edit.setInputType(InputType.TYPE_CLASS_TEXT);
        }
        return edit;
    }

    public RadioGroup createRadioGroup(int id, String[] q_List, int orientation) {
        RadioGroup rg = new RadioGroup(context);
        rg.setId(id);
        for (String s : q_List) {
            RadioButton rb = new RadioButton(context);
            rb.setText(s);
            rb.setTextColor(Color.BLACK);
            rg.addView(rb);
            rg.setOrientation(orientation);
            //rg.setLayoutParams(p);
        }
        return rg;
    }
}

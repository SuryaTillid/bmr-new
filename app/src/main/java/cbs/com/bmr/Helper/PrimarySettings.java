package cbs.com.bmr.Helper;

import android.content.Context;
import android.content.SharedPreferences;

/*********************************************************************
 * Created by Barani on 16-04-2019 in TableMateNew
 ***********************************************************************/
public class PrimarySettings {

    final String IS_CONFIGURED = "isConfigured";
    final String IP_ADDRESS = "ipAddress";
    private final String PREFERENCE_NAME = "BMR_Primary_Pref";
    private final int MODE = 0;
    private Context context;
    private SharedPreferences sPref;
    private SharedPreferences.Editor editor;

    public PrimarySettings(Context context) {
        this.context = context;
        sPref = context.getSharedPreferences(PREFERENCE_NAME, MODE);
        editor = sPref.edit();
    }

    public boolean isConfigured() {
        return sPref.getBoolean(IS_CONFIGURED, false);
    }

    public void setIS_CONFIGURED() {
        editor.putBoolean(IS_CONFIGURED, true);
        editor.commit();
    }

    public String getIP_ADDRESS() {
        return sPref.getString(IP_ADDRESS, null);
    }

    public void setIP_ADDRESS(String ipAddress) {
        editor.putString(IP_ADDRESS, ipAddress);
        editor.commit();
    }

    public void clearSession() {
        editor.clear();
        editor.commit();
    }
}
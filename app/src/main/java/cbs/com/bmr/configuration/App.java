package cbs.com.bmr.configuration;

import android.app.Application;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import cbs.com.bmr.Helper.TypefaceUtil;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import retrofit2.Retrofit;
import okhttp3.Response;
import okio.Buffer;
import retrofit2.converter.gson.GsonConverterFactory;

/*********************************************************************
 * Created by Barani on 21-03-2019 in TableMateNew
 ***********************************************************************/
public class App extends Application {
    private static String SERVER_ADDRESS;
    public static String IP_ADDRESS = "https://bmr.codebase.bz";
    private RestApi restApi;
    private static App mInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        //TypefaceUtil.overrideFonts(this);
    }

    public static synchronized App getInstance() {
        return mInstance;
    }

    public RestApi createRestAdaptor() {
        setServerAddress();

        Gson gson = new GsonBuilder().setLenient().create();

        Retrofit client = new Retrofit.Builder().baseUrl(SERVER_ADDRESS).client(getClient())
                .addConverterFactory(GsonConverterFactory.create(gson)).build();
        restApi = client.create(RestApi.class);

        return restApi;
    }

    private void setServerAddress() {
        if (AppLog.isDebugUrl)
            SERVER_ADDRESS = IP_ADDRESS;// getResources().getString(R.string.debug_url);
        else
            //SERVER_ADDRESS = getResources().getString(R.string.debug_url);
            SERVER_ADDRESS = IP_ADDRESS;
    }

    private OkHttpClient getClient() {
        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(5, TimeUnit.MINUTES).readTimeout(5, TimeUnit.MINUTES).writeTimeout(5, TimeUnit.MINUTES).addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Interceptor.Chain chain) throws IOException {
                        Response response = chain.proceed(chain.request());
                        AppLog.info("RestRequest", new Gson().toJson(response.request().url()));
                        RequestBody body = chain.request().body();
                        AppLog.info("RestResult", new Gson().toJson(response.code()));
                        Buffer buffer = new Buffer();
                        if (body != null) {
                            body.writeTo(buffer);
                            String bodya = buffer.readUtf8();
                            AppLog.info("RestInput", "" + bodya);
                        }
                        return response;
                    }
                }).build();
        return client;
    }
}
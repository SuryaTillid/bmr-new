package cbs.com.bmr.configuration;

import java.util.ArrayList;

import cbs.com.bmr.model.CustomerList;
import cbs.com.bmr.model.EmployeeInfoForManagementRequest;
import cbs.com.bmr.model.EmployeeList;
import cbs.com.bmr.model.FeedbackQuestions;
import cbs.com.bmr.model.ManagementRequestList;
import cbs.com.bmr.model.SuccessMessage;
import cbs.com.bmr.model.TODO_Taskslist;
import cbs.com.bmr.model.TaskList;
import cbs.com.bmr.model.ZoneMaster;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/*********************************************************************
 * Created by Barani on 21-03-2019 in TableMateNew
 ***********************************************************************/
public interface RestApi {

    @FormUrlEncoded
    @POST("index.php/tasksummary/createtaskapi")
    Call<SuccessMessage> createTask(@Field("task_date") String task_date, @Field("task_type") String task_type,
                                    @Field("customer_id") String customer_id, @Field("description") String description);

    @FormUrlEncoded
    @POST("index.php/tasksummary/updatetasksummaryapi")
    Call<SuccessMessage> updateTaskSummary(@Field("task_id") String task_id,@Field("td_id") String td_id,@Field("task_date") String task_date,
                                           @Field("created_by_id") String created_by_id,
                                           @Field("approved_by") String approved_by_id,
                                           @Field("created_date") String created_date, @Field("taskSchedule") String task);

   /* @GET("index.php/tasksummary/getlisttaskapi/{emp_id}")
    Call<ArrayList<TaskList>> getTaskList(@Path("emp_id") String emp_id);*/

    @FormUrlEncoded
    @POST("index.php/tasksummary/getlisttaskapi")
    Call<ArrayList<TaskList>> getTaskList(@Field("emp_id") String emp_id, @Field("region_id") String region_id,
                                          @Field("from_date") String from_date,@Field("to_date") String to_date);

    @GET("index.php/customers/getlist")
    Call<ArrayList<CustomerList>> getCustomerList();

    @GET("index.php/regionalzonemaster/getlistapi")
    Call<ArrayList<ZoneMaster>> getZoneList();

    @GET("index.php/employee/getlistapi")
    Call<ArrayList<EmployeeList>> getEmployee_List();

    @GET("index.php/employee/getdepartmentlistapi")
    Call<ArrayList<EmployeeInfoForManagementRequest>> getDepartment_List();

    @GET("index.php/feedbackquestion/getlist")
    Call<ArrayList<FeedbackQuestions>> getFeedbackQuestions(@Query("type") String type);

    @FormUrlEncoded
    @POST("/index.php/tasksummary/createtasksummaryapi")
    Call<SuccessMessage> createTaskSummary(@Field("task_id") String task_id,@Field("task_date") String task_date,
                                           @Field("created_by_id") String created_by_id,
                                           @Field("approved_by") String approved_by_id,
                                           @Field("assigned_by_id") String assigned_by_id,
                                            @Field("created_date") String created_date,@Field("taskSchedule") String taskSchedule);

    @FormUrlEncoded
    @POST("/index.php/feedbackQuestion/feedbackquestionsubmitapi")
    Call<SuccessMessage> feedbackSubmit(@Field("td_id") String td_id,@Field("fb_submit_time") String fb_submit,
                                        @Field("fb_geo_json") String fb_geo,
                                        @Field("feedBack") String feedback,
                                        @Field("login_id") String emp_id,@Field("fb_comments") String comments);

    @FormUrlEncoded
    @POST("/index.php/employee/loginapi")
    Call<SuccessMessage> login_bmr(@Field("username") String username,@Field("password") String password);

    @FormUrlEncoded
    @POST("/index.php/employee/createattendance")
    Call<SuccessMessage> employee_in_create_attendance(@Field("empid") String emp_id,@Field("check_in") String check_in,
                                                       @Field("geo_checkin") String geo_in,@Field("starting_meter") String starting_meter);

    @FormUrlEncoded
    @POST("/index.php/employee/updateattendance")
    Call<SuccessMessage> employee_out_update_attendance(@Field("empid") String emp_id,@Field("update_id") String update_id,
                                                        @Field("check_out") String check_out,
                                                        @Field("geo_checkout") String geo_checkout,@Field("closing_meter") String closing_meter);

    @FormUrlEncoded
    @POST("index.php/tasksummary/tasksummarycheckinapi")
    Call<SuccessMessage> check_in_task(@Field("td_id") String id,@Field("check_in") String check_in,
                                       @Field("geo_checkin") String geo_in,@Field("login_id") String emp_id);

    @FormUrlEncoded
    @POST("index.php/tasksummary/tasksummarycheckoutapi")
    Call<SuccessMessage> check_out_task(@Field("td_id") String id,@Field("check_out") String check_in,
                                        @Field("geo_checkout") String geo_out,@Field("login_id") String emp_id);

    @GET("index.php/taskSummary/gettodolistapi")
    Call<ArrayList<TODO_Taskslist>> getTodo_Tasks_list();

    @FormUrlEncoded
    @POST("index.php/taskSummary/gettaskdataapi")
    Call<ArrayList<TODO_Taskslist>> get_Task_By_ID(@Field("task_id") String task_id);

    @FormUrlEncoded
    @POST("index.php/taskSummary/createtodotaskapi")
    Call<SuccessMessage> create_Task_TodoList(@Field("task_text") String task_text,@Field("task_due") String task_due,
                                              @Field("task_notes") String task_notes,@Field("created_by") String created_by);

    @FormUrlEncoded
    @POST("index.php/taskSummary/updatetodotaskapi")
    Call<SuccessMessage> update_Task_TodoList(@Field("task_id") String task_id,@Field("task_text") String task_text,@Field("task_due") String task_due,
                                              @Field("task_notes") String task_notes);

    @FormUrlEncoded
    @POST("index.php/taskSummary/deletetaskapi")
    Call<SuccessMessage> delete_task(@Field("task_id") String task_id);

    @FormUrlEncoded
    @POST("index.php/tasksummary/updatetaskstatusapi")
    Call<SuccessMessage> check_approved_status(@Field("user_id") String user_id,@Field("taskid") String task_id,
                                               @Field("approvedstatus") String approved_status);

    @GET("index.php/managementrequest/getlist/{id}")
    Call<ArrayList<ManagementRequestList>> getRequestsList(@Path("id") String id);

    @FormUrlEncoded
    @POST("index.php/managementRequest/createRequest")
    Call<SuccessMessage> createManagementRequest(@Field("req_by") String emp_id,@Field("req_dep") String dept_id,
                                                 @Field("req_desc") String req_desc,@Field("comments")String comments,
                                                 @Field("req_qty") String req_qty,@Field("expected_budget") String expect_budget);

    @FormUrlEncoded
    @POST("index.php/managementRequest/updateRequest")
    Call<SuccessMessage> updateManagementRequest(@Field("id") String c_id,@Field("req_by") String emp_id,@Field("req_dep") String dept_id,
                                                 @Field("req_desc") String req_desc,@Field("comments")String comments,
                                                 @Field("req_qty") String req_qty,@Field("expected_budget") String expect_budget);

    @FormUrlEncoded
    @POST("index.php/managementRequest/deleteRequest")
    Call<SuccessMessage> deleteManagementRequest(@Field("req_id") String request_id);

    @GET("index.php/managementRequest/getemployeedepartment/{id}")
    Call<EmployeeInfoForManagementRequest> getEmployeeInfoList(@Path("id") String id);

    @GET("index.php/employee/dayinverification")
    Call<SuccessMessage> getDayInStatus(@Query("emp_id") String emp_id);

    @FormUrlEncoded
    @POST("index.php/employee/changepasswordapi")
    Call<SuccessMessage> changePassword(@Field("emp_id") String emp_id,@Field("password") String password);

    /*
    * Send Mail
    * */
    @FormUrlEncoded
    @POST("index.php/employee/sendcustomermail")
    Call<SuccessMessage> sendMail(@Field("guest_email") String guest_email,@Field("emp_name") String emp_name,
                                  @Field("task_id") String customer_name,@Field("comments") String comments);
}